
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" ng-app="taskwatch">
	<head>
	<title>Taskwatch: Zaplanuj swój następny ruch</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="theme-color" content="#00D9D9">
	<?php
	session_start();
	if(!isset($_SESSION['user_session']))
	{
		header("Location: discover.php");
	}

	include_once 'database/dbconfig.php';
	require('libraries/libraries.php');
	$stmt = $db_con->prepare("SELECT * FROM uzytkownicy WHERE user_id=:uid");
	$stmt->execute(array(":uid"=>$_SESSION['user_session']));
	$row=$stmt->fetch(PDO::FETCH_ASSOC);



	?>
	<link rel="stylesheet" type="text/css" href="css/tw-style.css">
	<link href="https://fonts.googleapis.com/css?family=Glegoo|Lato|Roboto+Slab:light" rel="stylesheet">

	<script>
	var user_id = <?php echo $row['user_id']; ?>;
	var pokaz_kolejne = <?php echo $row['pokaz_kolejne']; ?>;
	var pokaz_ukonczone_projekty = <?php echo $row['pokaz_ukonczone_projekty']; ?>;
	var pokaz_ukonczone_zadania = <?php echo $row['pokaz_ukonczone_zadania']; ?>;
	var terminy_styl = <?php echo $row['styl_terminow']; ?>;
	console.log("PHP user_id = " + user_id);
	
	/*
	console.log("pokaz_kolejne = " + pokaz_kolejne);
	console.log("pokaz_ukonczone_projekty = " + pokaz_ukonczone_projekty);
	console.log("pokaz_ukonczone_zadania = " + pokaz_ukonczone_zadania);
	console.log("terminy_styl = " + terminy_styl);
	*/
	</script>

</head>

<body ng-controller="tablicaController as tablice">



<?php //require('libraries/gtm-top-body.php'); ?>



<div class="pasek">
	<img src="images/logo-taskwatch.png" style="position: fixed; left: 10px;" title="Zaplanuj swój następny ruch" ng-click="pokazPanel()">

	<div class="ukryj" style="display: inline-block">
		<a href="#!/t/"><button class="akcja">Tablice</button></a>
		<a href="#!/przeglad"><button class="akcja">Przegląd</button></a>
		<a href="#!/metodyka"><button class="akcja">Metodyka</button></a>
	</div>



	<div class="ukryj doprawej" style="display: inline-block; margin-top: 5px">
		<button class="akcja" ng-click="pokazPanel()">
			Ustawienia wyświetlania
		</button>
		<a href="logout.php">
			<button class="akcja">
				<span class="glyphicon glyphicon-log-out"></span>&nbsp;Wyloguj
			</button>
		</a>
	</div>
</div>

<!-- ustawienia aplikacji -->
<div class="pelnyblok ustawienia" id="panel" ng-show="user.pokaz_panel">
	
		

		<div class="naglowek">
			Ustawienia układu:
		</div>
		<div>
		<button class="ustawienie">
				<span class="wyrownaj">	
				Sortuj projekty wg.:
				</span>
				<select ng-model="sortowanieProjektowWybor" ng-options="opcja as opcja.name for opcja in sortowanieProjektowOpcje" style="border-width: 0px; border-left: 1px solid black; height: 27px;">

			</select>
			</button>
			<br>

			<button class="ustawienie" ng-click="osobnePriorytety()">
				<span class="wyrownaj">
					Osobne priorytety:
				</span>
					{{user.priorytety_osobno === 0 ? "Nie" : "Tak"}}
				</span>
			</button>
			<br>
			<button class="ustawienie" ng-click="ukryjNiepriorytetowe()">
				<span class="wyrownaj">
				Ukryj niepriorytetowe:
				</span>
				{{user.ukryj_niepriorytetowe === 0 ? "Nie" : "Tak"}}
			</button>
			<br>
			<button class="ustawienie" ng-click="orientacjaProjektow()">
				<span class="wyrownaj">
				Orientacja bloków:
				</span>
				 {{user.projekty_orientacja}}
			</button>
			<br>
			<button class="ustawienie" ng-click="orientacjaPriorytetow()">
				<span class="wyrownaj">
				Orientacja priorytetów:
				</span>
				 {{user.priorytety_orientacja}}
			</button>


			<br>


		</div>

		<div class="naglowek">
				Ustawienia zadań:
			</div>
		<div>
			<button class="ustawienie">
					<span class="wyrownaj">	
						Ukryj poziomy:
					</span>
					<input type="number" ng-model="user.ukryj_poziomy" class="ustawienie" max="3" min="0" style="width: 64px">
					</button>
			<br>
			<button class="ustawienie" ng-click="pokazKolejne()">
				<span class="wyrownaj">
				Pokaz kolejne:
				</span>
				 {{user.pokaz_kolejne === 0 ? "Tak" : "Nie"}}
			</button>
			<br>

			<button class="ustawienie" ng-click="pokazUkonczoneZadania()">
				<span class="wyrownaj">		
				Pokaz ukonczone zadania:
				</span>
				{{user.pokaz_ukonczone_zadania === 0 ? "Tak" : "Nie"}}
			</button>
			<br>
			<button class="ustawienie" ng-click="pokazUkonczoneProjekty()">
			<span class="wyrownaj">	
				Pokaz ukonczone projekty:
				</span>
				 {{user.pokaz_ukonczone_projekty === 0 ? "Tak" : "Nie"}}
			</button>
			<br>
			<button class="ustawienie" ng-click="projektyZawijanie()">
			<span class="wyrownaj">	
				Projekty zawijanie:
				</span>
				{{user.projekty_zawijanie === 0 ? "Tak" : "Nie"}}
			</button>


		</div>

	


</div>

<div class="pelnyblok" ng-class="filtr" id="glownyobszar">

	<!-- routing - ng-view -->
	<div ng-view  >

	</div>



</div>
<!-- The Modal -->

<div id="myModal" class="modal" style="display: none" ng-click="zamknijEdycje(); $event.stopPropagation();">

  <!-- Modal content -->
  <div class="modal-content" ng-click="zamknijEdycje()">

	<projekt-edycja>Projekt edytowany</projekt-edycja>

  </div>

</div>

	<!-- test nowych dyrektyw -->
	<!--
	<button class="akcja" ng-click="ukryjPodpoziomy(0)">0</button>
	<button class="akcja" ng-click="ukryjPodpoziomy(1)">1</button>
	<button class="akcja" ng-click="ukryjPodpoziomy(2)">2</button>
	<button class="akcja" ng-click="ukryjPodpoziomy(3)">3</button>
	<button class="akcja" ng-click="ukryjPodpoziomy(4)">4</button>

	<div ng-repeat="z in zadania">
		<zadanie>test</zadanie>
	</div>
	<hr>
-->
<div style="display:block; height: 200px">
	&nbsp;


</div><!--

<div class="footer">
	Radosław Walasek | Rafał Włodarczyk | Rafał Wolsztyniak | Tomasz Wyrwał


</div>
-->
</body>
</html>