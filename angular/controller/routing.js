var tw = angular.module('taskwatch', ['ngRoute']);

var zadania;
var taby;
var projekty;

// DYREKTYWY



tw.directive('projekt', function(){
    return{
        restrict: 'E',
        templateUrl: 'angular/directive/projekt.html'
        //,scope: {}
    };
});

tw.directive('projektNowy', function(){
    return{
        restrict: 'E',
        templateUrl: 'angular/directive/projekt-nowy.html'
        //,scope: {}
    };
});

tw.directive('projektEdycja', function(){
    return{
        restrict: 'E',
        templateUrl: 'angular/directive/projekt-edycja.html'
        //,scope: {}
    };
});

tw.directive('tablica', function(){
    return{
        restrict: 'E',
        templateUrl: 'angular/directive/tablica.html'
    };
});

tw.directive('tablicaNowa', function(){
    return{
        restrict: 'E',
        templateUrl: 'angular/directive/tablica-nowa.html'
        //,scope: {}
    };
});

tw.directive('zadanie1', function(){
    return{
        restrict: 'E',
        templateUrl: 'angular/directive/zadanie1.html'
        //,scope: {}
    };
});
tw.directive('zadanie2', function(){
    return{
        restrict: 'E',
        templateUrl: 'angular/directive/zadanie2.html'
        //,scope: {}
    };
});

tw.directive('zadanieNowe', function(){
    return{
        restrict: 'E',
        templateUrl: 'angular/directive/zadanie-nowe.html'
        //,scope: {}
    };
});



// ROUTING

angular.module('taskwatch').config(function($routeProvider, $locationProvider){
    $routeProvider.when('/', {
        templateUrl: 'angular/template/tablice/index.html'
        //templateUrl: '<h1>inline template</h1>'
    })
    .when('/t/', {
        templateUrl: 'angular/template/tablice/index.html'
    })
    .when('/t/:id', {
        templateUrl: 'angular/template/tablica/test.html'
    })
    .when('/przeglad', {
        templateUrl: 'angular/template/przeglad/index.html'
    })
    .when('/terminy', {
        templateUrl: 'angular/template/przeglad/index.html'
    })
    .when('/metodyka', {
        templateUrl: 'angular/template/metodyka/index.html'
    })
    .otherwise({redirectTo: '/t/'});
});

