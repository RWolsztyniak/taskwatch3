
tw.controller('tablicaController', function tablicaController($scope, $parse, $rootScope, $routeParams, $http){
    
    $scope.sortowanieProjektowOpcje = [
        {id: 'projekt_kolejnosc', name: 'Kolejności'},
        {id: '-projekt_kolejnosc', name: 'Kolejności (odwrotnie)'},
        {id: 'projekt_utworzono', name: 'Daty dodania (rosnąco)'},
        {id: '-projekt_utworzono', name: 'Daty dodania (malejoąco)'},
    ];
    $scope.sortowanieProjektowWybor = $scope.sortowanieProjektowOpcje[0];

    $scope.ukrywaniePoziomowOpcje = [
        {id: 0, name: "0"},
        {id: 1, name: "1"},
        {id: 2, name: "2"},
        {id: 4, name: "3"},
    ];

    $scope.sortowanieProjektowWybor = $scope.sortowanieProjektowOpcje[0];

    $scope.kit = 1;
    $scope.params = $routeParams;

    $scope.aktualnaTablica = $scope.params.id;
    //$scope.kit = parseInt($scope.params.id);
    console.log($scope.params.id);

    $scope.$on('$routeChangeStart', function(next, current) { 
        console.log(next);
        console.log(current);
        //$scope.kit = parseInt($scope.params.id);
        console.log("aktualna tablica: " + $scope.params.id);
      });

       /*
      $scope.routingowa = function(rid){
 
                console.log($scope.params.id);
                $scope.aktualnaTablica = $scope.params.id;
        
                console.log("aktualna tablica: " + $scope.aktualnaTablica);
        
                for (p = 0; p < $scope.taby.length; p++){
                    if($scope.taby[p].tablica_id == rid){
                        $scope.aktualnaTablica = $scope.taby[p].tablica_nazwa;
                        console.log($scope.aktualnaTablica);
                    };
                };
          
            };
   */  

    $scope.ukryjPodpoziomyNr = null;

$scope.onChange = function(){

    console.log($scope.sortowanieProjektowWybor);

};


    odczytUzytkownika();

    function odczytUzytkownika(){
        console.log("funkcja - odczyt uzytkownika");
        $http({
            url: 'database/operacje-uzytkownik.php',
            method: "POST",
            data: { 
                'user_id' : user_id,
                'operacja' : 'odczytywanie'
                }
        })
        .then(function(response) {
            var tabliceJSON = response.data;
            var transformacja = angular.fromJson(tabliceJSON);
            $scope.user = transformacja[0];
            $scope.user.priorytety_osobno = parseInt($scope.user.priorytety_osobno);
            $scope.user.pokaz_kolejne = parseInt($scope.user.pokaz_kolejne);
            $scope.user.pokaz_kolejne = parseInt($scope.user.pokaz_panel);
            //$scope.user.pokaz_panel = 0;
            $scope.user.pokaz_ukonczone_zadania = parseInt($scope.user.pokaz_ukonczone_zadania);
            $scope.user.pokaz_ukonczone_projekty = parseInt($scope.user.pokaz_ukonczone_projekty);
            $scope.user.projekty_zawijanie = parseInt($scope.user.projekty_zawijanie);
            $scope.user.ukryj_niepriorytetowe = parseInt($scope.user.ukryj_niepriorytetowe);
            $scope.user.ukryj_poziomy = parseInt($scope.user.ukryj_poziomy);

            var user = $scope.user;


            console.log($scope.user);
            console.log("priorytety osobno: " + $scope.user.priorytety_osobno);
            odczytTablic();
        }, 
        function(response) { // optional
            console.log("błąd");
            // failed
        });
        
    };

    function odczytTablic(){
        //console.log("funkcja - odczyt tablic");
        $http({
            url: 'database/operacje-tablice.php',
            method: "POST",
            data: { 
                'user_id' : 4,
                'operacja' : 'odczytywanie'
                }
        })
        .then(function(response) {
            var tabliceJSON = response.data;
            var transformacja = angular.fromJson(tabliceJSON);
            $scope.taby = transformacja;
            
            var taby = $scope.taby;
            console.log(transformacja);
            odczytProjektow();
        }, 
        function(response) { // optional
            console.log("błąd");
            // failed
        });
        
    };



    
    

    var kontrola = 0;


        // CRUD - Odczyt Projektów _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
    
        function odczytProjektow(){
            
            $http({
                url: 'database/operacje-projekty.php',
                method: "POST",
                data: { 
                    'user_id' : 4,
                    'operacja' : 'odczytywanie'
                    }
            })
            .then(function(response) {
                console.log(response.data);
                $scope.projekty = angular.fromJson(response.data);
                projekty = $scope.projekty;
                //console.log("funkcja - odczyt projektow");
                odczytZadan();
            }, 
            function(response) { // optional
                console.log("błąd - odczyt zadan");
                // failed
            });
            
        };

        // CRUD - Odczyt Przypomnien  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _

        function odczytPrzypomnien(){
            
            $http({
                url: 'database/operacje-przypomnienia.php',
                method: "POST",
                data: { 
                    'user_id' : 4,
                    'operacja' : 'odczytywanie'
                    }
            })
            .then(function(response) {
                console.log(response);
                var transformacja = angular.fromJson(response.data);
                $scope.przypomnienia = transformacja;
                //console.log("funkcja - odczyt przypomnien");
                console.log(transformacja);
                
            }, 
            function(response) { // optional
                console.log("błąd - odczyt przypomnien");
                // failed
            });
            
        };

        // CRUD - Odczyt Zadan  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _

        function odczytZadan(){
            
            $http({
                url: 'database/operacje-zadania.php',
                method: "POST",
                data: { 
                    'user_id' : 4,
                    'operacja' : 'odczytywanie'
                    }
            })
            .then(function(response) {
                var zadaniaJSON = response.data;
                var transformacja = angular.fromJson(zadaniaJSON);
                ustalanieWykonalnosci(transformacja);
                var zadania = transformacja;
                $scope.zadania = poziomowanie(transformacja);
                //console.log("funkcja - odczyt zadan");


                $scope.strukturaPoziomuZero;
                $scope.postepy;
                //console.log(transformacja);
                odczytPrzypomnien();
                
            }, 
            function(response) { // optional
                console.log("błąd - odczyt zadan");
                // failed
            });
            
        };


        // Nadawanie struktury zadaniom  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _

        function ustalanieWykonalnosci(tablica){

            for (z=0; z < tablica.length; z++){
                //console.log(tablica[z].zadanie_rodzic + ' / ' + tablica[z].zadanie_tresc);
                if(stopniowoscListy(tablica, tablica[z].zadanie_rodzic) == true){
                    
                    var nastepca = parseInt(tablica[z].zadanie_kolejnosc)+1;
                    //console.log(tablica[z].zadanie_kolejnosc + " -----> " + nastepca);
                    //console.log(tablica[z].zadanie_tresc + " -----> " + nastepca);
                    //ustalenieWykonalnosciNastepcy(tablica, tablica[z].zadanie_rodzic, nastepca);
                    tablica[z].zadanie_typrodzica = "s";
                    console.log()
                    
                } else {
                    tablica[z].zadanie_typrodzica = "r";
                    
                };
                
            };
            console.log("funkcja - ustalanieWykonalnosci");
            return tablica;
        };

        function stopniowoscListy(tablica, rodzic){
            var typRodzica;
            for (i=0; i < tablica.length; i++){

                if (tablica[i].zadanie_rodzic == rodzic){
                    if (tablica[i].zadanie_typdzieci == "s"){
                        return true;
                    };
                };
            };
            //return typRodzica;
        };

        //ustalenie typu dzieci podstawowego poziomu zadan w projekcie

        function strukturaPoziomuZero(){
            for(z = 0; z < $scope.zadania.length; z++){
                if($scope.zadania[z].zadanie_rodzic == 0){
                    for (p = 0; p < $scope.projekty; p++){
                        if($scope.zadania[z].zadanie_rodzic == $scope.projekty[p].projekt_id){
                            $scope.zadania[z].zadanie_typrodzica = $scope.projekty[p].projekt_typdzieci;
                        } else {
                            // ??
                        };
                    };
                };
            };
            console.log("funkcja - strukturaPoziomuZero");
        };


  /*      function ustalenieWykonalnosciNastepcy(tablica, rodzic, nastepca){
            for (n=0; n < tablica.length; n++){
                if(tablica[n].zadanie_rodzic == rodzic && tablica[n].zadanie_kolejnosc == nastepca){
                    console.log(tablica[rodzic].zadanie_tresc + ": " + tablica[n].zadanie_tresc);
                }
            };

        };
*/


    // Akcje wewnątrz projektów i zadań  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _

    // który projekt kliknelismy?

    $scope.edytujZadanie = function(zid){
        console.log("kliknelismy zadanie o id: " + zid);
    };

    $scope.edytujProjekt = function(pid){
        console.log("kliknelismy projekt o id: " + pid);
        $scope.edytowanyProjekt = pid;
        $scope.$apply;

        var zamknij = document.getElementById("zamknij");
        var span = document.getElementsByClassName("close")[0];
        var modal = document.getElementById('myModal');
        var glownyobszar = document.getElementById('glownyobszar');

        if(modal.style.display == "none"){
            modal.style.display = "block";
            $scope.filtr = "blur";
        } else {
            modal.style.display = "none";
            $scope.filtr = "";
        };
        console.log("ID edytowanego projektu: " + pid);
        console.log(typeof pid);
    };
    
    $scope.zamknijEdycje = function(){
        
            var zamknij = document.getElementById("zamknij");
            var span = document.getElementsByClassName("close")[0];
            var modal = document.getElementById('myModal');
            var glownyobszar = document.getElementById('glownyobszar');
    
            if(modal.style.display == "none"){
                modal.style.display = "block";
                $scope.filtr = "blur";
            } else {
                modal.style.display = "none";
                $scope.filtr = "";
            };
        };




    // Zmiana priorytetu projektu

    $scope.zmienPriorytet = function(pid2){
        //console.log("zmieniamy priorytet projektu " + pid2);
        for (i = 0; i < $scope.projekty.length; i++){
            if($scope.projekty[i].projekt_id == pid2){
                if($scope.projekty[i].projekt_priorytet != 1){
                    $scope.projekty[i].projekt_priorytet = 1;
                } else {
                    $scope.projekty[i].projekt_priorytet = 0;
                };
            };
        };
    };


    $scope.zmienPriorytetTablicy = function(tid2){
        //console.log("zmieniamy priorytet projektu " + pid2);
        for (i = 0; i < $scope.taby.length; i++){
            if($scope.taby[i].tablica_id == tid2){
                if($scope.taby[i].tablica_priorytet != 1){
                    $scope.taby[i].tablica_priorytet = 1;
                } else {
                    $scope.taby[i].tablica_priorytet = 0;
                };
            };
        };
    };

    // usuwanie zadan

    $scope.zadanieUsun = function(zid3){
        //console.log("zmieniamy dlugosc zadania " + zid2);
        /*
        for (i = 0; i < $scope.zadania.length; i++){
            if($scope.zadania[i].zadanie_id == zid3){
                $scope.zadania.splice(i,1);
            };
        };
        */
        $http({
            url: 'database/operacje-zadania.php',
            method: "POST",
            data: {
                'operacja' : 'usuwanie',
                'user_id' : user_id,
                'zadanie_id' : zid3,
                }
            }).then(function(response){
                var tekst = response.data;
                console.log(tekst);
                odczytZadan();
            },
            function(response){
                //failed
            }
        );
        odczytZadan();
    };

    // zadanieTypDzieci

    $scope.zadanieTypDzieci = function(zid4){
        //console.log("zmieniamy dlugosc zadania " + zid2);
        for (i = 0; i < $scope.zadania.length; i++){
            if($scope.zadania[i].zadanie_id == zid4){

            };
        };
    };


    // Oznaczanie zadań jako krótkich
    $scope.zmianaKrotkich = function(zid2){
        //console.log("zmieniamy dlugosc zadania " + zid2);
        for (i = 0; i < $scope.zadania.length; i++){
            if($scope.zadania[i].zadanie_id == zid2){
                if($scope.zadania[i].zadanie_krotkie != 1){
                    $scope.zadania[i].zadanie_krotkie = 1;
                } else {
                    $scope.zadania[i].zadanie_krotkie = 0;
                };
            };
        };
    };


    $scope.zadanieZmianaStanu = function(zid5){

        //console.log("zmieniamy dlugosc zadania " + zid2);
        
        for (i = 0; i < $scope.zadania.length; i++){
            if($scope.zadania[i].zadanie_id == zid5){
                if($scope.zadania[i].zadanie_stan != 1){
                    $scope.zadania[i].zadanie_stan = 1;
                } else {
                    $scope.zadania[i].zadanie_stan = 0;
                };
            };
        };
        $scope.postepy();
        
        //odczytZadan();
    };

    // projektTypDzieci

    $scope.projektTypDzieci = function(pid4){
        console.log("typ dzieci projektu: " + pid4);
        for (i = 0; i < $scope.projekty.length; i++){
            if($scope.projekty[i].projekt_id == pid4){
                if($scope.projekty[i].projekt_typdzieci != 'r'){
                    $scope.projekty[i].projekt_typdzieci = 'r';
                } else {
                    $scope.projekty[i].projekt_typdzieci = 's';
                };
                console.log($scope.projekty[i]);
            };
        };
    };

    // usuwanie projektu
    $scope.projektUsun = function(pid3){
        console.log("usuwamy projekt " + pid3);
        /*
        for (i = 0; i < $scope.projekty.length; i++){
            if($scope.projekty[i].projekt_id == pid3){
                $scope.projekty.splice(i,1);
            };
        };
        */
        $http({
            url: 'database/operacje-projekty.php',
            method: "POST",
            data: {
                'operacja' : 'usuwanie',
                'user_id' : user_id,
                'projekt_id' : pid3,
                }
            }).then(function(response){
                var tekst = response.data;
                odczytProjektow();
            },
            function(response){
                //failed
            }
        );

    };


    

    // POZIOMOWANIE
	// Celem tej funkcji jest ustalenie, czy dane zadanie ma jakieś zadanie nadrzędne
	// Pozwala to określić poziom, na którym ma być zadanie i zastosować odpowiednie wcięcia
	function poziomowanie(tablica){
		
		for (i=0; i < tablica.length; i++){

			
			if(tablica[i].zadanie_rodzic == "0"){
				//jeżeli jest to główny poziom - rodzic
				tablica[i].poziom = 0;
                tablica[i].adres = tablica[i].zadanie_kolejnosc + '-' + tablica[i].zadanie_rodzic + '-' + tablica[i].zadanie_id;
                //trzeba sprawdzić projekt_id
				
			} else if (tablica[i].zadanie_rodzic != "0"){
				//jeżeli jest to drugi poziom - dziecko
				tablica[i].poziom = 1;

				
				for (j=0; j < tablica.length; j++){
					
					if (tablica[j].zadanie_id == tablica[i].zadanie_rodzic){
						//odnalezienie rodzica
						
                            tablica[i].adres =tablica[j].zadanie_kolejnosc + '-' + tablica[i].zadanie_kolejnosc + '-' + tablica[i].zadanie_rodzic + '-' + tablica[i].zadanie_id;
                            
						
						if(tablica[j].zadanie_rodzic !="0"){
							//jeżeli jest to trzeci poziom - wnuk
							tablica[i].poziom = 2;
							
								for (k=0; k < tablica.length; k++){
									
									if (tablica[k].zadanie_id == tablica[j].zadanie_rodzic){
									tablica[i].adres = tablica[k].zadanie_kolejnosc + '-' + tablica[j].zadanie_kolejnosc + '-' + tablica[j].zadanie_rodzic + '-' + tablica[j].zadanie_id + '-' +tablica[i].zadanie_kolejnosc;	
									}//if
								} //for
						} //if
					}; //if
				}; //for
			}; //if
		}; //for	
	
		console.log(tablica);
		return tablica;
	}; //koniec funkcji - POZIOMOWANIE
    
    

    $scope.ukryjPodpoziomy = function(pp){
        $scope.ukryjPodpoziomyNr = pp;
        console.log($scope.ukryjPodpoziomyNr);
    };


    // CRUD - Dodawanie Tablic

    $scope.tablicaDodaj = function(){
        
        var tNazwa = document.getElementById("nowa-tablica-nazwa").value;
        var tOpis = document.getElementById("nowa-tablica-opis").value;
        var tablica_kolejnosc = 0;
        console.log("dodajemy: " + tNazwa + " " + tOpis);

        //okreslenie numeru kolejnej tablicy

        for(t = 0; t < $scope.taby.length; t++){
            if(tablica_kolejnosc < $scope.taby[t].tablica_kolejnosc){
                tablica_kolejnosc = $scope.taby[t].tablica_kolejnosc;
            };
        };
        tablica_kolejnosc++;
        console.log("nr kolejnej tablicy: " + tablica_kolejnosc);

        $http({
            url: 'database/operacje-tablice.php',
            method: "POST",
            data: {
                'operacja' : 'dodawanie',
                'user_id' : user_id,
                'tablica_nazwa' : tNazwa,
                'tablica_opis' : tOpis,
                'tablica_utworzono' : Date.now(),
                'tablica_kolejnosc' : tablica_kolejnosc
            }
        }).then(function(response){
            var tekst = response.data;
            odczytTablic();
        },
        function(response){
            //failed
        }
    );
};

    // CRUD - Dodawanie Tablic

    $scope.tablicaUsun = function(tidu){
        
        $http({
            url: 'database/operacje-tablice.php',
            method: "POST",
            data: {
                'operacja' : 'usuwanie',
                'user_id' : user_id,
                'tablica_id' : tidu,
            }
        }).then(function(response){
            var tekst = response.data;
            odczytTablic();
        },
        function(response){
            //failed
        }
    );
};

// TEST
// Pasek postępu w zadaniu

    $scope.postepy = function(){
        var liczbaZadan;
        var liczbaZadanSkonczonych;

        for (p = 0; p < $scope.projekty.length; p++){

            liczbaZadan = 0;
            liczbaZadanSkonczonych = 0;
            $scope.projekty[p].liczba_zadan = 0;

            for (z = 0; z < $scope.zadania.length; z++){
                
                if($scope.zadania[z].projekt_id == $scope.projekty[p].projekt_id){

                    liczbaZadan++;
                    if($scope.zadania[z].zadanie_stan == "1"){
                        liczbaZadanSkonczonych++;
                    }
                };
                $scope.projekty[p].liczba_zadan = liczbaZadan;
                $scope.projekty[p].liczba_zadan_skonczonych = liczbaZadanSkonczonych;
            };

            //console.log("dziecko " + liczbaZadan);
        };
        console.log($scope.projekty);
    };




    // WIDOK
    // Zmiany ustawien

    $scope.orientacjaProjektow = function(){

        if($scope.user.projekty_orientacja != "wiersz"){
            $scope.user.projekty_orientacja = "wiersz";
        } else {
            $scope.user.projekty_orientacja = "kolumna";
        };
        $scope.aktualizacjaUstawien();

        console.log("priorytety: " + $scope.user.priorytety_orientacja + " | projekty: " + $scope.user.projekty_orientacja);
    };


    $scope.orientacjaPriorytetow = function(){

        if($scope.user.priorytety_orientacja != "wiersz"){
            $scope.user.priorytety_orientacja = "wiersz";
        } else {
            $scope.user.priorytety_orientacja = "kolumna";
        };
        $scope.aktualizacjaUstawien();

        console.log("priorytety: " + $scope.user.priorytety_orientacja + " | projekty: " + $scope.user.projekty_orientacja);
    };

    $scope.osobnePriorytety = function(){
        
        if($scope.user.priorytety_osobno != 1){
            $scope.user.priorytety_osobno = 1;
        } else {
            $scope.user.priorytety_osobno = 0;
        };

        $scope.aktualizacjaUstawien();

        console.log("priorytety_osobno: " + $scope.user.priorytety_osobno);
    };

// RAGE
    $scope.pokazKolejne = function(){
        
        if($scope.user.pokaz_kolejne != 1){
            $scope.user.pokaz_kolejne = 1;
        } else {
            $scope.user.pokaz_kolejne = 0;
        };
        console.log("pokaz_kolejne: " + $scope.user.pokaz_kolejne);
        $scope.dbdebug();
        $scope.aktualizacjaUstawien();
    };

    $scope.pokazPanel = function(){
        
        if($scope.user.pokaz_panel != 1){
            $scope.user.pokaz_panel = 1;
        } else {
            $scope.user.pokaz_panel = 0;
        };

        //$scope.aktualizacjaUstawien();
    };

    $scope.pokazUkonczoneZadania = function(){
        
        if($scope.user.pokaz_ukonczone_zadania != 1){
            $scope.user.pokaz_ukonczone_zadania = 1;
        } else {
            $scope.user.pokaz_ukonczone_zadania = 0;
        };

        //$scope.aktualizacjaUstawien();
    };

    $scope.pokazUkonczoneProjekty = function(){
        
        if($scope.user.pokaz_ukonczone_projekty != 1){
            $scope.user.pokaz_ukonczone_projekty = 1;
        } else {
            $scope.user.pokaz_ukonczone_projekty = 0;
        };

        $scope.aktualizacjaUstawien();
    };
    $scope.projektyZawijanie = function(){
        
        if($scope.user.projekty_zawijanie != 1){
            $scope.user.projekty_zawijanie = 1;
        } else {
            $scope.user.projekty_zawijanie = 0;
        };

        $scope.aktualizacjaUstawien();
    };

    $scope.ukryjNiepriorytetowe = function(){
        
        if($scope.user.ukryj_niepriorytetowe != 1){
            $scope.user.ukryj_niepriorytetowe = 1;
        } else {
            $scope.user.ukryj_niepriorytetowe = 0;
        };

        $scope.aktualizacjaUstawien();
    };



    $scope.dbdebug = function(){
        $http({
            url: 'database/debug.php',
            method: "POST",
            data: { 
                'user_id' : user_id,
                'operacja' : 'aktualizacja',
                'priorytety_osobno' : $scope.user.priorytety_osobno,
                'priorytety_orientacja' : $scope.user.priorytety_orientacja,
                'projekty_orientacja' : $scope.user.projekty_orientacja,
                'pokaz_kolejne' : $scope.user.pokaz_kolejne,
                'pokaz_panel' : $scope.user.pokaz_panel,
                'pokaz_ukonczone_zadania' : $scope.user.pokaz_ukonczone_zadania,
                'pokaz_ukonczone_projekty' : $scope.user.pokaz_ukonczone_projekty,
                'projekty_zawijanie' : $scope.user.projekty_zawijanie,
                'ukryj_niepriorytetowe' : $scope.user.ukryj_niepriorytetowe
                }
        })
        .then(function(response) {
            console.log(response.data);
        }, 
        function(response) { // optional
            console.log("błąd");
            // failed
        });
    };

    $scope.aktualizacjaUstawien = function(){
        //console.log("zapisz ustawienia do bazy");

        $http({
            url: 'database/operacje-uzytkownik.php',
            method: "POST",
            data: { 
                'user_id' : user_id,
                'operacja' : 'aktualizacja',
                'priorytety_osobno' : $scope.user.priorytety_osobno,
                'priorytety_orientacja' : $scope.user.priorytety_orientacja,
                'projekty_orientacja' : $scope.user.projekty_orientacja,
                'pokaz_kolejne' : $scope.user.pokaz_kolejne,
                'pokaz_panel' : $scope.user.pokaz_panel,
                'pokaz_ukonczone_zadania' : $scope.user.pokaz_ukonczone_zadania,
                'pokaz_ukonczone_projekty' : $scope.user.pokaz_ukonczone_projekty,
                'projekty_zawijanie' : $scope.user.projekty_zawijanie,
                'ukryj_niepriorytetowe' : $scope.user.ukryj_niepriorytetowe
                }
        })
        .then(function(response) {
            console.log(response);
            odczytUzytkownika();
        }, 
        function(response) { // optional
            console.log("błąd");
            // failed
        });
    };
    // wyswietlanie akcji





    $scope.pokazAkcje = function(pidA){
        //console.log("fruuuuu");
    };





});