
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" ng-app="taskwatch">
	<head>
	<title>Taskwatch 3.0</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="theme-color" content="#00D9D9">
	<?php
	session_start();
	if(!isset($_SESSION['user_session']))
	{
		header("Location: discover.php");
	}

	include_once 'database/dbconfig.php';
	require('libraries/libraries.php');
	$stmt = $db_con->prepare("SELECT * FROM uzytkownicy WHERE user_id=:uid");
	$stmt->execute(array(":uid"=>$_SESSION['user_session']));
	$row=$stmt->fetch(PDO::FETCH_ASSOC);
	?>
	<link rel="stylesheet" type="text/css" href="css/tw-style.css">
	<link href="https://fonts.googleapis.com/css?family=Glegoo|Lato|Roboto+Slab:light" rel="stylesheet">

	<script>
	var user_id = <?php echo $row['user_id']; ?>;
	var pokaz_kolejne = <?php echo $row['pokaz_kolejne']; ?>;
	var pokaz_ukonczone_projekty = <?php echo $row['pokaz_ukonczone_projekty']; ?>;
	var pokaz_ukonczone_zadania = <?php echo $row['pokaz_ukonczone_zadania']; ?>;
	var terminy_styl = <?php echo $row['styl_terminow']; ?>;
	console.log("user_id = " + user_id);
	/*
	console.log("pokaz_kolejne = " + pokaz_kolejne);
	console.log("pokaz_ukonczone_projekty = " + pokaz_ukonczone_projekty);
	console.log("pokaz_ukonczone_zadania = " + pokaz_ukonczone_zadania);
	console.log("terminy_styl = " + terminy_styl);
	*/
	</script>

</head>

<body ng-controller="tablicaController as tablice">



<?php //require('libraries/gtm-top-body.php'); ?>



<div class="pasek">
	<img src="images/logo-taskwatch.png" title="Zaplanuj swój następny ruch">
	<div class="doprawej">
		<a href="logout.php">
			<span class="glyphicon glyphicon-log-out"></span>&nbsp;Wyloguj
		</a>
	</div>
</div>



<div class="pelnyblok">

<div style="display: flex; flex-direction:row">
	<div class="blok projekt">
		<input type="number" value="1" ng-model="kit">
		<br>
		{{kit}} <br>
		{{aktualnaTablica}} - aktualna tablica<br> 
		{{params.id}} - aktualna tablica<br> 
	</div>

	<div class="blok projekt">
		<p>
		<label>
		Ukryj od poziomu:
			<input type="range" name="points" min="0" max="10">
		</label><br>
		<label><input type="checkbox" ng-checked="user.pokaz_ukonczone_projekty">Ukryj zakończone</label><br>
		<label><input type="checkbox" ng-checked="user.pokaz_ukonczone_zadania">Ukryj kolejne kroki</label><br>
		</p>
	</div>

</div>




<hr>
<h3>Tablice</h3>

<div style="display: flex; flex-direction:row">
		<tablica ng-repeat="t in taby">x</tablica>
	
	</div>



	<hr>
		<h3>Projekty</h3>
        

        

	<hr>
	ng-view:
    <a href="#!tablice">tablice</a>
    <a href="#!tablica">tablica</a>
	<a href="#!metodyka">metodyka</a>

	<button class="akcja" ng-click="modal()">modal</button>
<hr>

	<!-- test nowych dyrektyw -->
	<!--
	<button class="akcja" ng-click="ukryjPodpoziomy(0)">0</button>
	<button class="akcja" ng-click="ukryjPodpoziomy(1)">1</button>
	<button class="akcja" ng-click="ukryjPodpoziomy(2)">2</button>
	<button class="akcja" ng-click="ukryjPodpoziomy(3)">3</button>
	<button class="akcja" ng-click="ukryjPodpoziomy(4)">4</button>

	<div ng-repeat="z in zadania">
		<zadanie>test</zadanie>
	</div>
	<hr>
-->

	<h4 class="center">Projekty priorytetowe</h4>

	<div style="display: flex; flex-direction:row">
		<projekt ng-repeat="p in projekty | filter: {projekt_priorytet: 1} | filter: {tablica_id: aktualnaTablica}"></projekt>
	
	</div>
	<hr>
	<h4 class="center">Projekty zwykłe</h4>
	<div style="display: flex; flex-direction:row">
		<projekt ng-repeat="p in projekty | filter: {projekt_priorytet: 0} | filter: {tablica_id: aktualnaTablica}" ></projekt>
	
	</div>




	<!-- routing - ng-view -->
	<div ng-view  >

	</div>
	<hr>


</div>
<!-- The Modal -->
<div id="myModal" class="modal" style="display: none" ng-click="modal(); $event.stopPropagation();">

  <!-- Modal content -->
  <div class="modal-content" ng-click="modal()">
    <button class="akcja" ng-click="modal()">Zamknij</button>
    <p>{{projekty[aktualnyPID].projekt_tresc}}</p>
  </div>

</div>

</body>
</html>