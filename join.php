

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
	session_start();

	if(isset($_SESSION['user_session'])!="")
	{
		header("Location: home.php");
	}
	require('libraries/libraries.php');
	?>


<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TaskWatch: Rejestracja</title>


<link href="libraries/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="libraries/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" media="screen"> 
<script type="text/javascript" src="libraries/jquery-1.11.3-jquery.min.js"></script>
<script type="text/javascript" src="validation.min.js"></script>
<link href="css/tw-marketing.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" src="login.js"></script>

</head>

<body>

<nav class="navbar">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>              
      </button>
      <a class="navbar-brand" href="index.php"><img src="images/logo-taskwatch.png" style="margin-left:15px"></a>
    </div>
  </div>
</nav>


<div class="signin-form">

	<div class="container">
     
        
       <form class="form-signin karta" method="post" id="register-form">
      
        <h2 class="form-signin-heading">Rejestracja</h2><hr />
        
        <div id="error">
        <!-- error will be showen here ! -->
        </div>
        
        <div class="form-group">
        <input type="email" class="form-control" placeholder="Adres e-mail" name="user_email" id="user_email" /><label style="font-size: 12px;margin-top:5px; font-weight: normal">Adres e-mail będzie używany jako <br>identyfikator do logowania</label>
        <span id="check-e"></span>
        </div>

        <div class="form-group">
        <input type="text" class="form-control" placeholder="Imię" name="user_name" id="user_name" />
        </div>

        <div class="form-group">
        <input type="password" class="form-control" placeholder="Hasło" name="password" id="password" />
        </div>
        
        <div class="form-group">
        <input type="password" class="form-control" placeholder="Powtórz hasło" name="cpassword" id="cpassword" />
        </div>
     	<hr />
        
        <div class="form-group">
            <button type="submit" class="btn btn-default btn-block" name="btn-save" id="btn-submit">
				<span class="glyphicon glyphicon-log-in"></span> &nbsp; Stwórz konto
			</button> 
			<hr>
			<a href="index.php">
				<button type="button" class="btn btn-default btn-block" name="btn-save" id="btn-button">
					<span class="glyphicon glyphicon-log-in"></span> &nbsp; Przejdz do aplikacji
				</button>
			</a>
        </div>  
      
      </form>

    </div>
    
</div>
    
<script src="libraries/bootstrap/js/bootstrap.min.js"></script>

</body>
</html>