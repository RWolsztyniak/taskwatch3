
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" ng-app="taskwatch">
	<head>
	<title>TaskWatch: Zaplanuj swój następny ruch</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />



	<link href="https://fonts.googleapis.com/css?family=Glegoo|Lato|Roboto+Slab:light" rel="stylesheet">

	<script type="text/javascript" src="../libraries/jquery-1.11.3-jquery.min.js"></script>

	<script type="text/javascript" src="../libraries/jqueryui/jquery-ui.min.js"></script>
	<link href="../libraries/jqueryui/jquery-ui.min.css" rel="stylesheet">
	<link href="../libraries/jqueryui/jquery-ui.theme.min.css" rel="stylesheet">

	<link href="../libraries/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="../libraries/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" media="screen">
	<link rel="stylesheet" type="text/css" href="../css/tw-marketing.css">



  <!-- jqplot  -->
  <link class="include" rel="stylesheet" type="text/css" href="jquery.jqplot.min.css" />

  <link type="text/css" rel="stylesheet" href="syntaxhighlighter/styles/shCoreDefault.min.css" />
  <link type="text/css" rel="stylesheet" href="syntaxhighlighter/styles/shThemejqPlot.min.css" />


</head>
<body>



<form  class="container-fluid" >
	<div  class"row">

		<div class="projekt">
			<p>Date from: <input type="text" id="from" name="from" value="<?php echo $zadania_od = date('Y-m-d',strtotime('-7 days'));?>"></p>
		</div>

    <div class="projekt">
      <p>Date to: <input type="text" id="to" name="to" value="<?php echo $zadania_do = date('Y-m-d')?>" ></p>
    </div>
		<div class="projekt">
		<select name="radio_zadania" id="radio_zadania" value= "<?php echo $liczba_zadania = 1?>">
			<option>1</option>
			<option>2</option>
			<option>5</option>
			<option>10</option>

		</select>
		</div>
		<div class="projekt">
    <p><input type="button" value="Prześlij" name="button_zadania" id="button_zadania" ></p>
	</div>


	</div>
</form>
<hr>
<div id="chart1" style="height:300px; width:70%; margin-left: 4cm; ">
</div>
<hr>
<div id="chart2" style="height:300px; width:74%; margin-left: 2.7cm; ">
</div>
<hr>
<div id="chart3" style="height:300px; width:70%; margin-left: 4cm; ">
</div>


<script type="text/javascript">
    $(document).ready(function(){
				wykres();

			$('#button_zadania').click(function(){
				wykres();

			});

			function wykres() {

				var from = $('#from').val();
				var to = $('#to').val();
				var radio_zadania = $('#radio_zadania').val();
				var liczba = radio_zadania * 15;




				if (from > to){

					alert("Data początkowa nie może być większa od daty końcowej.")
				}


			// Wykres 2

			$.ajax({
			type: 'POST',
			url: 'wykres2.php',
			dataType: 'json',
			data: {'from': from, 'to': to},

				success:function(json) {
								var plot1
								var line2= json;




								 plot1 = $.jqplot('chart2', [line2], {
									title:'Ilośc rejestracji na dzień',
									series: [{
										neighborThreshold: 1
								}],

									axes:{
											xaxis:{

													renderer:$.jqplot.DateAxisRenderer,

													label: "Data rejestracji użytkownika",

													tickOptions:{
														formatString:'%d/%m/%Y',
														fontSize:'10pt',
														fontFamily:'Tahoma',



												}
											},
											yaxis: {
												renderer: $.jqplot.LogAxisRenderer,
												label: "Liczba użytkowników",
												tickOptions:{formatString: '%d'},
												min: 0,
												max: liczba,
												tickInterval: radio_zadania,
												fontSize:'10pt',
												fontFamily:'Tahoma',

											}
									},
									series:[{ lineWidth:4, markerOptions:{ style:'square' } }],
											cursor:{
													zoom:true,
													looseZoom: true
											}
								});
									plot1.redraw();

								},

								error: function(blad) {
									alert("Wystąpił problem z wykresem skontaktuj się z administratorem systemu. Kod błędu: 1" );

								}

			});

			// Wykres 1
			$.ajax({
			type: 'POST',
			url: 'wykres1.php',
			dataType: 'json',
			data: {'from': from, 'to': to},

				success:function(json) {
								var plot1
								var line1= json;




								 plot1 = $.jqplot('chart1', [line1], {
									title:'Ilość zadań na dzień',
									series: [{
										neighborThreshold: 1
								}],

									axes:{
											xaxis:{

													renderer:$.jqplot.DateAxisRenderer,

													label: "Data dodania zadania",

													tickOptions:{
														formatString:'%d/%m/%Y',
														fontSize:'10pt',
														fontFamily:'Tahoma',



												}
											},
											yaxis: {
												renderer: $.jqplot.LogAxisRenderer,
												label: "Liczba zadań",
												tickOptions:{formatString: '%d'},
												min: 0,
												max: liczba,
												tickInterval: radio_zadania,
												fontSize:'10pt',
												fontFamily:'Tahoma',

											}
									},
									series:[{ lineWidth:4, markerOptions:{ style:'square' } }],
											cursor:{
													zoom:true,
													looseZoom: true
											}
								});
									plot1.redraw();

								},

								error: function(blad) {
									alert("Wystąpił problem z wykresem skontaktuj się z administratorem systemu. Kod błędu: 1" );

								}

			});

			// Wykres 3

									$.ajax({
									type: 'POST',
									url: 'wykres3.php',
									dataType: 'json',
									data: {'from': from, 'to': to},

										success:function(json) {
														var plot3
														var line3= json;




														 plot1 = $.jqplot('chart3', [line3], {
															title:'Wzrost liczby aktywowanych kont użytkowników',
															series: [{
																neighborThreshold: 1
														}],

															axes:{
																	xaxis:{

																			renderer:$.jqplot.DateAxisRenderer,

																			label: "Data",

																			tickOptions:{
																				formatString:'%d/%m/%Y',
																				fontSize:'10pt',
																				fontFamily:'Tahoma',


																		}
																	},
																	yaxis: {
																		renderer: $.jqplot.LogAxisRenderer,
																		label: "Liczba kont",
																		tickOptions:{formatString: '%d'},
																		min: 0,
																		max: liczba,

																		fontSize:'10pt',
																		fontFamily:'Tahoma',

																	}
															},
															series:[{ lineWidth:4, markerOptions:{ style:'square' } }],
																	cursor:{
																			zoom:true,
																			looseZoom: true
																	}
														});
															plot1.redraw();

							},

							error: function(blad) {
								alert("Wystąpił problem z wykresem skontaktuj się z administratorem systemu. Kod błędu: 2");

							}

					});

			};

			});

			</script>

<!-- datepicker -->

			<script>

			  $( function() {
			    var
			        from = $( "#from" )
			        .datepicker({
			          dateFormat: 'yy-mm-dd',
			          defaultDate: "+1w",
			          changeMonth: true,
			          changeYear: true,
			          numberOfMonths: 1
			        })
			        .on( "change", function() {
			          to.datepicker( "option", "minDate", getDate( this ) );
			        }),
			      to = $( "#to" ).datepicker({
			        dateFormat: 'yy-mm-dd',
			        defaultDate: "+1w",
			        changeMonth: true,
			        changeYear: true,
			        numberOfMonths: 1
			      })
			      .on( "change", function() {
			        from.datepicker( "option", "maxDate", getDate( this ) );
			      });

			    function getDate( element ) {
			      var date;
			      try {
			        date = $.datepicker.parseDate({dateFormat: 'dd-mm-yy'},  element.value );
			      } catch( error ) {
			        date = null;
			      }

			      return date;
			    }
			  } );

			</script>

<!-- Don't touch this! -->


    <script class="include" type="text/javascript" src="jquery.jqplot.min.js"></script>
    <script type="text/javascript" src="syntaxhighlighter/scripts/shCore.min.js"></script>
    <script type="text/javascript" src="syntaxhighlighter/scripts/shBrushJScript.min.js"></script>
    <script type="text/javascript" src="syntaxhighlighter/scripts/shBrushXml.min.js"></script>
		<link rel="stylesheet" type="text/css" href="jquery.jqplot.css" />
<!-- End Don't touch this! -->

<!-- Additional plugins go here -->

    <script class="include" language="javascript" type="text/javascript" src="jqplot.dateAxisRenderer.min.js"></script>

<!-- End additional plugins -->
</body>
</html>
