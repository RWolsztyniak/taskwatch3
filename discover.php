
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<?php


	session_start();

	if(isset($_SESSION['user_session'])!="")
	{
		header("Location: index.php");
	}
	//require('libraries/gtm-top-head.php');
	require('libraries/libraries.php');
	?>
	<title>TaskWatch: Zaplanuj swój następny ruch</title>


	<link href="libraries/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="libraries/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" media="screen">

	<link href="css/tw-marketing.css" rel="stylesheet" type="text/css" media="screen">
	<script type="text/javascript" src="libraries/jquery-1.11.3-jquery.min.js"></script>
	<script type="text/javascript" src="validation.min.js"></script>
	<script type="text/javascript" src="login.js"></script>
	
	
	
</head>
<body>
<?php //require('libraries/gtm-top-body.php'); ?>
<!-- nawigacja -->



<nav class="navbar">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>              
      </button>
      <a class="navbar-brand" href="index.php"><img src="images/logo-taskwatch.png" style="margin-left:15px"></a>
    </div>
  </div>
</nav>





<!-- głowna część -->
<div class="signin-form">

	<div class="container">
     
        
       <form class="form-signin karta" method="post" id="login-form">
      
        <h2 class="form-signin-heading">Witamy</h2><hr />
        
        <div id="error">
        <!-- error will be shown here ! -->
        </div>
        
        <div class="form-group">
        <input type="email" class="form-control" placeholder="Adres email" name="user_email" id="user_email" />
        <span id="check-e"></span>
        </div>
        
        <div class="form-group">
        <input type="password" class="form-control" placeholder="Hasło" name="password" id="password" />
        </div>
			<button type="submit" class="btn btn-default btn-block " name="btn-login" id="btn-login">
				<span class="glyphicon glyphicon-log-in"></span> &nbsp; Zaloguj
			</button> 
     	<hr />
        
        <div class="form-group">
            <p>Nie masz jeszcze konta?</p>
    		

			<a href="join.php" class="btn btn-default btn-block">
			<span class="glyphicon glyphicon-log-in"></span> &nbsp; Zarejestruj się
			</a> 			
			
        </div>  
      
      </form>

    </div>
    
</div>
<hr>


<hr>




    
<script src="libraries/bootstrap/js/bootstrap.min.js"></script>

</body>
</html>