
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" ng-app="taskwatch">
	<head>
	<title>Taskwatch 3.0</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="theme-color" content="#00D9D9">
	
	<?php

	session_start();

	if(!isset($_SESSION['user_session']))
	{
		header("Location: discover.php");
	}

	include_once 'database/dbconfig.php';
	require('libraries/libraries.php');
	$stmt = $db_con->prepare("SELECT * FROM uzytkownicy WHERE user_id=:uid");
	$stmt->execute(array(":uid"=>$_SESSION['user_session']));
	$row=$stmt->fetch(PDO::FETCH_ASSOC);

	?>

	
	<!-- angular -->
	<script src="libraries/angular.min.js"></script>
	<script src="libraries/angular-route.min.js"></script>
	<script src="libraries/angular-resource.min.js"></script>
	<script src="libraries/angular-animate.min.js"></script>

	<!-- angular -->
	<script src="angular/taskwatch.js"></script>

	<link rel="stylesheet" type="text/css" href="css/tw-style.css">
	<link href="https://fonts.googleapis.com/css?family=Glegoo|Lato|Roboto+Slab:light" rel="stylesheet">

	<script>
	var user_id = <?php echo $row['user_id']; ?>;
	var pokaz_kolejne = <?php echo $row['pokaz_kolejne']; ?>;
	var pokaz_ukonczone_projekty = <?php echo $row['pokaz_ukonczone_projekty']; ?>;
	var pokaz_ukonczone_zadania = <?php echo $row['pokaz_ukonczone_zadania']; ?>;
	var terminy_styl = <?php echo $row['styl_terminow']; ?>;
	console.log("user_id = " + user_id);
	console.log("pokaz_kolejne = " + pokaz_kolejne);
	console.log("pokaz_ukonczone_projekty = " + pokaz_ukonczone_projekty);
	console.log("pokaz_ukonczone_zadania = " + pokaz_ukonczone_zadania);
	console.log("terminy_styl = " + terminy_styl);
	</script>


</head>
<body ng-controller="tablicaController as tablice">

    <div class="tw-layout">

        <div class="tw-panel tw-menu" style="order: 1">
            <div class="tw-nav">
                <img src="images/ikona-taskwatch.png">
            </div><!-- menu panel - top-->
            <div>
            ng-view:
                <a href="#!/"><button class="menu-button">test</button></a>

                <a href="#!tablice"><div class="menu-button">tablice</div></a>
                <a href="#!tablica"><div class="menu-button">tablica</div></a>
                <a href="#!metodyka"><div class="menu-button">metodyka</div></a>
            </div>
            <hr>

            <!-- lista tablic-->
            <div >
            
                <div ng-repeat="t in taby">
                    <a href="#!metodyka">
                        <div class="menu-button">
                        {{t.tablica_nazwa}}
                            </div>
                        </a>
                </div>

            </div>
            <!-- / lista tablic-->      
            <hr>
             <!-- lista ustawien-->
             <div>
            Ustawienia
                <div class="menu-button">
                    <label>
                        <input type="checkbox" ng-model="a">
                        Ukryj zakończone
                    </label>


                    </div>
                <div class="menu-button">
                    <label>
                        <input type="checkbox" ng-model="b">
                        Ukryj kolejne kroki
                        </label>


                    </div>
                <div class="menu-button">
                    <label>
                    Nie wyświetlaj poziomu
                        <input type="number" ng-model="c" min=0 max=3 value=0>
                        </label>
                    </div>
                </div>

            <!-- /lista ustawien-->     



        </div><!-- menu panel - containter-->

        <div class="tw-panel" style="order: 2; flex-basis: 500px" ng-view> 


    
        </div><!-- środek -->

        <div class="tw-panel" style="order: 3">
            <div class="tw-nav">
                prawy
            </div><!-- prawy panel - top-->
            
            <button class="akcja" ng-click="ukryjPodpoziomy(0)">0</button>
            <button class="akcja" ng-click="ukryjPodpoziomy(1)">1</button>
            <button class="akcja" ng-click="ukryjPodpoziomy(2)">2</button>
            <button class="akcja" ng-click="ukryjPodpoziomy(3)">3</button>
            <button class="akcja" ng-click="ukryjPodpoziomy(4)">4</button>
        </div><!-- prawy panel -->

    </div><!-- layout -->


</body>
</html>