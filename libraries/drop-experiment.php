
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" ng-app="taskwatch">
	<head>
	<title>Taskwatch 3.0</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="theme-color" content="#00D9D9">


	<!-- angular -->
	<script src="libraries/angular.min.js"></script>
	<script src="libraries/angular-route.min.js"></script>
	<script src="libraries/angular-resource.min.js"></script>
	<script src="libraries/angular-animate.min.js"></script>

	<!-- angular -->
	<script src="javascript/taskwatch.js"></script>


	<link rel="stylesheet" type="text/css" href="css/tw-style.css">
	<style>
		ul{
			border: 0px solid lightgrey;
			margin: 2px;
		}
	</style>

</head>
<body ng-controller="tablicaController as tablice">
	<h3>Taskwatch 3.0</h3>
	<hr>
	<input type="number" id="user" value="4">

<hr>
<h3>Tablice</h3>
		<div ng-repeat="t in taby">
			<div class="blok" ng-class="{alpha:$first, omega:$last}">
				<input type="text" value="{{t.tablica_nazwa}}">
				<button>e</button>
				<br>
				<input type="textarea" value="{{t.tablica_opis}}">
				<button>x</button>
			</div>
			<br>
			<div class="blok" ng-class="{alpha:$first, omega:$last}" ng-if="$last">
				<input type="text">
				<button>e</button>
				<br>
				<input type="textarea">
				<button>+</button>
			</div>
		</div>
	<hr>
		<h3>Projekty</h3>
        
        <label><input type="checkbox" ng-model="ukrywanieZakonczonych">Ukryj zakończone</label><br>
        <label><input type="checkbox" ng-model="ukrywanieKolejnych">Ukryj kolejne kroki</label><br>
        
        
        <div class="tablica">
		<div ng-repeat="p in projekty" >
			<div class="blok projekt" ng-class="{alpha:$first, omega:$last}">
				<input type="text" value="{{p.projekt_tresc}}">
				<button>e</button>
				<br>
				<input type="textarea" value="{{p.projekt_stan}}">
				<button>x</button>
                <hr>
                <div>
                    <div ng-repeat="z in zadania | filter: {projekt_id: p.projekt_id}">
                        <div class="zadanie" ng-class="{alpha:$first, omega:$last}">
                            <input type="text" value="{{z.zadanie_tresc}}">
                            <button>e</button>
                            <br>
                            <input type="textarea" value="{{z.zadanie_stan}}">
                            <button>x</button>
                        </div>
                        <br>
                        <div class="zadanie" ng-class="{alpha:$first, omega:$last}" ng-if="$last">
                            <input type="text" placeholder="Dodaj zadanie">
                            <button>e</button>
                            <br>
                            <input type="textarea">
                            <button>+</button>
                        </div>
                    </div>
                 </div>

			</div>
            </div>
			<br>
            <!-- dodawanie -->
			<div class="blok" ng-class="{alpha:$first, omega:$last}" ng-if="$last">
				<input type="text" placeholder="Dodaj projekt">
				<button>e</button>
				<br>
				<input type="textarea">
				<button>+</button>
			</div>
		</div>
		<hr>

		<hr>



	{{tablice.zmienna}}
	</div>
	<div>
		<p ng-controller="projektController as projekt">


		{{projekt.zmienna}}
		</p>
		
		<p ng-controller="zadanieController as zadanie">
		{{zadanie.zmienna}}
		
		

		</p>
	
		<p ng-controller="terminController as termin">
		{{termin.zmienna}}
		</p>
	
		<p ng-controller="userController as user">
		{{user.zmienna}}
		</p>
	
	
	</div>


	<hr>
	ng-view:
	<a href="#!">test</a>
	<a href="#!tablica">tablica</a>

	<!-- routing -->
	<div ng-view>

	</div>
	<hr>

	
</body>
</html>